package cucumberJava;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
public class ClasseCucumber {

//page objet a completer
    @FindBy(xpath = "https://petstore.octoperf.com/actions/Catalog.action")
    protected WebElement connexion;

    @FindBy(xpath = "//a[normalize-space()='Sign In']")
    protected WebElement login;

    @FindBy(xpath = "//input[@name='username']")
    protected WebElement username;

    @FindBy(xpath = "//input[@name='password']")
    protected WebElement psw;

    @FindBy(xpath = "//input[@name='signon']")
    protected WebElement click;


    public WebElement getConnexion() {
        return connexion;
    }

    public WebElement getLogin() {
        return login;
    }

    public WebElement getUsername() {
        return username;
    }

    public WebElement getPsw() {
        return psw;
    }

    public WebElement getClick() {
        return click;
    }

}
